import React from 'react'

import SEO from '../../shared/components/seo/seo'
import Home from '../../layout-default/pages/home/home'

export default () => (
  <>
    <SEO title="Home" />

    <Home />
  </>
)
