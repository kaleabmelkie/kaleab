import React from 'react'

import './titles.scss'

type TitlesProps = {}

const Titles: React.FC<TitlesProps> = ({}) => {
  return (
    <>
      <h1 className="title">
        <span className="first-name">kaleab</span>{' '}
        <abbr title="serekebrhan">s.</abbr> melkie
      </h1>

      <sub className="subtitle">
        co-founder &amp;&amp; ceo,{' '}
        <a href="https://www.kelaltech.com/">kelal tech plc</a>
      </sub>
    </>
  )
}

export default Titles
